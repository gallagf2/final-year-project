
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ShortestPath {
	
	private double aMatrix[][] = new double[12479][12479]; 
	
	private String stop_times, transfers; // stop.txt and transfers.txt
	public final int numberOfStops = 12479;
	int value = Integer.MAX_VALUE;
	double max = Double.POSITIVE_INFINITY;
	public final double oneHundred = 100; // needed for when the transfer type = 2
	
	ShortestPath(String stop_times, String transfers) 
	{
		this.stop_times = stop_times;
		this.transfers = transfers;
		
		//building our matrix - will create a class 
		try {
			buildingMatrix();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();// error checking if file is not found
		}
	}


private void buildingMatrix() throws FileNotFoundException {
		
	File stopTimes = new File("stop_times.txt");
    File transferFile = new File("transfers.txt");
	
	//matrix to infinity
		for(int r = 0; r < aMatrix.length; r++) {
			for(int t = 0; t < aMatrix[r].length; t++) {
				if(r != t) {
					aMatrix[r][t] = max;
				}
				else {
					aMatrix[r][t] = 0;
				}
			}
		}
		
		File stopT = new File(stop_times);
		Scanner text = new Scanner(stopT);
		Scanner line = null;
		
		text.nextLine();
		text.nextLine();// used to skip the first line of the file and access the information provided 
		
		int begins = 0, ends = 0, 
		routePrevious = 0, routeId = 0;
		double cost;
		String Line;
		cost = 1;
		while(text.hasNextLine()) // as told in the question the cost of this is given as 1 - stop time file . While loop is used to scan through the large file
		{ 
			Line = text.nextLine();
			line = new Scanner(Line);
			line.useDelimiter(",");
			
			routePrevious = routeId;
			routeId = line.nextInt();
			
			line.next();
			line.next();// skip line
			
			begins = ends;
			ends = line.nextInt();
			
			if(routePrevious == routeId) {
				aMatrix[begins][ends] = cost;
			}
			line.close();
		}
		text.close();
		
		int transfer; 
		double minTime;
		File transfersFile = new File(transfers);
		text = new Scanner(transfersFile);
		text.nextLine(); // used to skip the first line of the file and access the information provided
		
		// choice of a while loop to scan through transfers.txt file 
		while(text.hasNextLine()) { 
			Line = text.nextLine();
			line = new Scanner(Line);
			// scans from current position until it finds a line seperator delimiter
			line.useDelimiter(",");
			
			begins = line.nextInt();
			ends = line.nextInt();
			transfer = line.nextInt();
			
			if(transfer == 0) {
				aMatrix[begins][ends] = 2; // when the cost is 2 -> the transfer type is 0 
			}
			else if(transfer == 2) 
			{
				minTime = line.nextDouble();
				aMatrix[begins][ends] = minTime / oneHundred; // when transfer type -> we make the cost the minimum time divided by the integer 100
			}
			line.close(); // closes scanner
		}
		text.close(); // closes text scanner
	}	
	
// code taken from the slides - graphs shortest path lecture slide 13
//@https://tcd.blackboard.com/bbcswebdav/pid-2208617-dt-content-rid-13154097_1/courses/CSU22012-202122/graphs%20-%20shortest%20paths%20lecture.pdf

private void relax(int begins, int ends, double[] distTo, int[] e) {
	if(distTo[ends] > distTo[begins] + aMatrix[begins][ends]) {
		distTo[ends] = distTo[begins] + aMatrix[begins][ends];
		e[ends] = begins;
	}
}

public String shortestDistance(int begins, int ends){
	
	if(begins == ends) 
	{
		return "" + aMatrix[begins][ends] + " via " + begins;
	}
	
	int visited[] = new int[aMatrix.length];
	
	double distTo[] = new double[aMatrix.length];
	int e[] = new int[aMatrix.length];
	
	// for loop is used to set all values to infinity to scan through
	for(int r = 0; r < distTo.length; r++) 
	{
		if(r != begins) // starting node not set to infinity
		{
			distTo[r] = max;
		}
	}
	
	// used a similar version to assignment 2 - dijkstras algorthim
	visited[begins] = 1;
	distTo[begins] = 0;  //starting node
	int Node = begins; 
	// current node is at the start
	
	int nodesVisited = 0; // to begin dijkstras algorthim we set the total nodes at the start to 0 
	while(nodesVisited < distTo.length)
	{
		//we relax the edges that direct towards the node we are on - this is then set as a visited node and added to nodes visited 
		for(int r = 0; r < aMatrix[Node].length; r ++) 
		{
			if(!Double.isNaN(aMatrix[Node][r]) && visited[r] == 0) {
    			relax(Node, r, distTo, e);
    		}
		}
		visited[Node] = 1; // visited then adds on 1
		 
		// following this we then find the node that has the shortest distance from our current node 
		// this node must not have been visited before 
		// we then add this onto out total nodes visited
		double shortestDistToNode = value;
		for(int r = 0; r < distTo.length; r++) 
		{
			if(visited[r] != 1 && shortestDistToNode > distTo[r]) 
			{
				Node = r;
				shortestDistToNode = distTo[r];
			}
		}
		nodesVisited++; // added to nodes visited
	}
	
	//now since we have the information we must now build the path
	if(distTo[ends] == max) // if paths goes to infinity it is not possible
	{
		return "this path is not possible";
	}
	
	int b= begins;
	int ed = ends;
	
	String path = "";
	while(ed != b) {
		path =  ":" + e[ed] + path; // path goes from : : 
		ed = e[ed];
	}
	path = path + ":" + ends;
	
	return distTo[ends] + " via " + path;
}

}
