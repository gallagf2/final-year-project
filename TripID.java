
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

 class StopTimesFile {

	static List<TripInfo> p;

	StopTimesFile(String stopTimes) 
	{
		File file;
		try {
			if (stopTimes != null) 
			{
				p = new ArrayList<>();
				
				double shapeDistanceTravelled = -1;
				file = new File(stopTimes);
				
				Scanner scan = new Scanner(file);
				scan.nextLine();//skipping the first line to access all the information 
				
				while (scan.hasNextLine())
				{ // used to check if there is another line in this input 
					
					String trip[] = scan.nextLine().split("");  
                    
					int id = (trip[0] != "") ? Integer.parseInt(trip[0]): -1; Time arrivalTime = Time.valueOf(trip[1]); Time departureTime = Time.valueOf(trip[2]);int stopId = (trip[3] != "") ? Integer.parseInt(trip[3]) : -1;
					int stopSequence = (trip[4] != "") ? Integer.parseInt(trip[4]) : -1;int stopHeadsign = (trip[5] != "") ? Integer.parseInt(trip[5]) : -1;int pickupType = (trip[6] != "") ? Integer.parseInt(trip[6]) : -1;
					int dropoffType = (trip[7] != "") ? Integer.parseInt(trip[7]) : -1;
					
					try {
					shapeDistanceTravelled = (trip[8] != "") ? Double.parseDouble(trip[8]) : -1;
					
					}
					catch(ArrayIndexOutOfBoundsException e1) {
						shapeDistanceTravelled = -1;
						
					}
					p.add(new TripInfo(id, arrivalTime, departureTime, stopId, stopSequence, stopHeadsign, pickupType, dropoffType, shapeDistanceTravelled));
				}
				scan.close();
			} else {

			}

		} catch (FileNotFoundException e) {
			System.out.println("File cannot be located");
		}
	}
