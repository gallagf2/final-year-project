
import java.util.Scanner;
import java.util.List;

public class FrontInterface {

	public static void main(String[] args) {
		
		ShortestPath graph = new ShortestPath("stop_times.txt", "transfers.txt");
		
		System.out.println("Welcome to Vancouver Bus system.");
		System.out.println("Please be patient while this program runs");
		System.out.println("Enter A if you would like this system to find the shortest path between your two chosen 2 bus stops");
		System.out.println("Enter B if you would like this system to search for a bus stop");
		System.out.println("Enter C if you would like this system to find all trips in a given arrival time");
		System.out.println("Enter exit if you would like to leave the Vancouver Bus System");
		Scanner input = new Scanner (System.in)	;
		
		boolean complete = false;
		while(!complete)
		{
			if  (input.hasNextLine())
			{
				if (input.hasNext("exit")) // if exit is entered you will leave the system
				{
					System.out.println("Thank you for using Vancouver Bus system.");
					complete = true ;
				}
				else if(input.hasNext("A") || input.hasNext("a"))
				{
					input.nextLine(); //move the scanner past A entered

					boolean A = true ;
					while(A== true )
					{
						System.out.println("Please input two bus ID's (seperted by -)."
								+ "Enter exit to leave Vancouver Bus System or back to return to the previous options.");
						if (input.hasNextLine())	
						{
							String userInput =input.nextLine();
							String[] busId = userInput.split("-");

							if (userInput.equalsIgnoreCase("exit"))	
							{
								complete = true;
								A = false ;
								System.out.println("Thank you for using Vancouver Bus system."); 
							}
							else if(userInput.equalsIgnoreCase("back"))
							{
								System.out.println("Enter A to get shortest path, B to search  bustops, C to find all trips by arrival time or exit to leave this system ");
								A = false ;
							}
							else if(busId.length < 2 || busId.length >2) 
							{
								System.out.println("This in an invalid input");
							}
							else if (busId.length == 2)
							{
								try {
									int firstStop = Integer.parseInt(busId[0]);
									int secondStop = Integer.parseInt(busId[1]);

									System.out.println("Shortest path from " + firstStop + " to " + secondStop + " is " + graph.shortestDistance(firstStop, secondStop));
									
								}
								catch(NumberFormatException n)
								{
									System.out.println("Enter in numbers only !");	
								}
							}
							else 
							{
								System.out.println("Please enter two integers seperated by - ");
							}
						}	
					}
				}

				else if(input.hasNext("B") || input.hasNext("b"))
				{	
					input.nextLine();
					boolean B = true;

					while(B == true )
					{
						System.out.println("Search for a bus stop by full name or the first few characters."
								+ "Enter exit to leave Vancouver Bus System or back to return to the previous options."
								+ "");
						if (input.hasNextLine())	
						{
							String userInputB =input.nextLine();

							if (userInputB.equalsIgnoreCase("exit"))	
							{
								complete = true;
								B = false ;
								System.out.println("Thank you for using Vancouver bus system."); 
							}
							else if(userInputB.equalsIgnoreCase("back"))
							{
								System.out.println("Enter A for shortest path, B to search by bustops, C to find all trips by arrival time or quit to exit the application");
								B = false ;
							}
							else 
							{
								
								{
									System.out.println("" );
								}			
							}
						}	
					}
				}
				else if(input.hasNext("C") || input.hasNext("c"))
					{
						{
							System.out.println("Error: unable to calculate sorry for this problem");
					
				{
					
					input.nextLine();
				}
			}
		}

		input.close();

	}

	

		}
	}
		}
				